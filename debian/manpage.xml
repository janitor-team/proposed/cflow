<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE refentry PUBLIC '-//OASIS//DTD DocBook XML V4.5//EN' 'http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd'
[
    <!ENTITY p 'cflow'>
    <!ENTITY version '1.2'>
]>

<refentry>

<refentryinfo>
    <title>&p; manual</title>
    <productname>&p;</productname>
    <author>
        <firstname>Jakub</firstname> <surname>Wilk</surname>
        <contrib>Wrote this manpage for the Debian system.</contrib>
        <address><email>jwilk@debian.org</email></address>
    </author>
    <copyright>
        <year>1997</year>
        <year>2005</year>
        <year>2007</year>
        <holder>Sergey Poznyakoff</holder>
    </copyright>
    <copyright>
        <year>2009</year>
        <holder>Jakub Wilk</holder>
    </copyright>
    <legalnotice>
        <para>
            This manual page was written for the Debian system (and may be used by others).
        </para>
        <para>
            Permission is granted to copy, distribute and/or modify this document under the terms of the GNU General
            Public License Version 3, as published by the Free Software Foundation.
        </para>
        <para>
            On Debian systems, the complete text of the GNU General Public License Version 3 can be found in
            <filename>/usr/share/common-licenses/GPL-3</filename>.
        </para>
    </legalnotice>
</refentryinfo>

<refmeta>
    <refentrytitle>&p;</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class='version'>&version;</refmiscinfo>
</refmeta>

<refnamediv>
    <refname>&p;</refname>
    <refpurpose>analyze control flow in C source files</refpurpose>
</refnamediv>

<refsynopsisdiv>
    <cmdsynopsis>
        <command>&p;</command>
        <arg choice='opt' rep='repeat'><replaceable>option</replaceable></arg>
        <arg choice='plain' rep='repeat'><replaceable>file</replaceable></arg>
    </cmdsynopsis>
    <cmdsynopsis>
        <command>&p;</command>
        <group choice='req'>
            <arg choice='plain'><option>--help</option></arg>
            <arg choice='plain'><option>--usage</option></arg>
            <arg choice='plain'><option>--version</option></arg>
            <arg choice='plain'><option>-V</option></arg>
        </group>
    </cmdsynopsis>
</refsynopsisdiv>

<refsection>
    <title>Description</title>
    <para>
        This manual page documents briefly the <command>&p;</command> command.
    </para>
    <para>
        This manual page was written for the Debian distribution because the original program does not have a manual
        page.  Instead, it has documentation in the GNU
        <citerefentry>
            <refentrytitle>info</refentrytitle>
            <manvolnum>1</manvolnum>
        </citerefentry> format.
    </para>
    <para>
        <application>GNU cflow</application> analyzes a collection of C source files and prints a graph, charting
        control flow within the program.
    </para>
    <para>
        <application>GNU cflow</application> is able to produce both direct and inverted flowgraphs for C sources.
        Optionally a cross-reference listing can be generated. Two output formats are implemented: POSIX and GNU
        (extended).
    </para>
    <para>
        Input files can optionally be preprocessed before analyzing.
    </para>
</refsection>

<refsection>
    <title>Options</title>
    <para>
        A summary of options is included below. For a complete description, see the
        <citerefentry>
            <refentrytitle>info</refentrytitle>
            <manvolnum>1</manvolnum>
        </citerefentry> files.
    </para>
    <para>
        The program follows the usual GNU command line syntax, with long options starting with two dashes
        (<literal>-</literal>).
        Mandatory or optional arguments to long options are also mandatory or
        optional for any corresponding short options.
    </para>
    <para>
        The effect of each option marked with an asterisk is reversed if the option's
        long name is prefixed with ‘<literal>no-</literal>’. For example, ‘<literal>--no-cpp</literal>’ cancels
        ‘<literal>--cpp</literal>’.
    </para>
    <refsection>
        <title>General options</title>
        <variablelist>
        <varlistentry>
            <term><option>-d</option></term>
            <term><option>--depth=<replaceable>number</replaceable></option></term>
            <listitem>
                <para>Set the depth at which the flowgraph is cut off.</para>
            </listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-f</option></term>
            <term><option>--format=<replaceable>name</replaceable></option></term>
            <listitem>
                <para>
                    Use given output format <replaceable>name</replaceable>.  Valid names are ‘<literal>gnu</literal>’
                    (default) and ‘<literal>posix</literal>’.
                </para>
            </listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-i</option></term>
            <term><option>--include=<replaceable>classes</replaceable></option></term>
            <listitem>
                <para>
                    Include specified classes of symbols (see below). Prepend <replaceable>classes</replaceable> with
                    <literal>^</literal> or <literal>-</literal> to exclude them from the output.
                </para>
                <para>
                    Symbols classes:
                    <variablelist>
                    <varlistentry>
                        <term><literal>_</literal></term>
                        <listitem><para>symbols whose names begin with an underscore</para></listitem>
                    </varlistentry>
                    <varlistentry>
                        <term><literal>s</literal></term>
                        <listitem><para>static symbols</para></listitem>
                    </varlistentry>
                    <varlistentry>
                        <term><literal>t</literal></term>
                        <listitem><para>typedefs (for cross-references only)</para></listitem>
                    </varlistentry>
                    <varlistentry>
                        <term><literal>x</literal></term>
                        <listitem><para>all data symbols, both external and static</para></listitem>
                    </varlistentry>
                    </variablelist>
                </para>
            </listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-o</option></term>
            <term><option>--output=<filename><replaceable>file</replaceable></filename></option></term>
            <listitem>
                <para>
                    Set output file name. Default is <filename>-</filename>, meaning standard output.
                </para>
            </listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-r</option></term>
            <term><option>--reverse</option></term>
            <listitem><para>* Print reverse call graph.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-x</option></term>
            <term><option>--xref</option></term>
            <listitem><para>Produce cross-reference listing only.</para></listitem>
        </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Parser control</title>
        <variablelist>
        <varlistentry>
            <term><option>-a</option></term>
            <term><option>--ansi</option></term>
            <listitem>
                <para>* Accept only sources in ANSI C.</para>
            </listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-D</option></term>
            <term><option>--define=<replaceable>name[=defn]</replaceable></option></term>
            <listitem><para>Predefine <replaceable>name</replaceable> as a macro.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-I</option></term>
            <term><option>--include-dir=<replaceable>dir</replaceable></option></term>
            <listitem>
                <para>
                    Add the directory <replaceable>dir</replaceable> to the list of directories to be searched for
                    header files.
                </para>
            </listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-m</option></term>
            <term><option>--main=<replaceable>name</replaceable></option></term>
            <listitem><para>Assume main function to be called <replaceable>name</replaceable>.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-p</option></term>
            <term><option>--pushdown=<replaceable>number</replaceable></option></term>
            <listitem><para>Set initial token stack size to <replaceable>number</replaceable>.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>--preprocess<replaceable>[=command]</replaceable></option></term>
            <term><option>--cpp<replaceable>[=command]</replaceable></option></term>
            <listitem><para>* Run the specified preprocessor command.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-s</option></term>
            <term><option>--symbol=<replaceable>symbol</replaceable>:<replaceable>type</replaceable></option></term>
            <listitem>
                <para>
                    Register <replaceable>symbol</replaceable> with given <replaceable>type</replaceable>. Valid types
                    are: ‘<literal>keyword</literal>’ (or ‘<literal>kw</literal>’), ‘<literal>modifier</literal>’,
                    ‘<literal>identifier</literal>’, ‘<literal>type</literal>’, ‘<literal>wrapper</literal>’. Any
                    unambiguous abbreviation of the above is also accepted.
                </para>
            </listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-S</option></term>
            <term><option>--use-indentation</option></term>
            <listitem><para>* Use source file indentation as a hint.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-U</option></term>
            <term><option>--undefine=<replaceable>name</replaceable></option></term>
            <listitem><para>Cancel any previous definition of <replaceable>name</replaceable>.</para></listitem>
        </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Output control</title>
        <variablelist>
        <varlistentry>
            <term><option>-b</option></term>
            <term><option>--brief</option></term>
            <listitem><para>* Use brief output.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>--emacs</option></term>
            <listitem><para>* Additionally format output for use with GNU Emacs.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-l</option></term>
            <term><option>--print-level</option></term>
            <listitem><para>* Print nesting level along with the call graph.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>--level-indent=<replaceable>string</replaceable></option></term>
            <listitem>
                <para>
                    Use <replaceable>string</replaceable> when indenting to each new level.
                </para>
            </listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-n</option></term>
            <term><option>--number</option></term>
            <listitem><para>* Print line numbers.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>--omit-arguments</option></term>
            <listitem><para>* Do not print argument lists in function declarations.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>--omit-symbol-names</option></term>
            <listitem><para>* Do not print symbol names in declaration strings.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-T</option></term>
            <term><option>--tree</option></term>
            <listitem><para>* Draw ASCII art tree.</para></listitem>
        </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Informational options</title>
        <variablelist>
        <varlistentry>
            <term><option>--debug<replaceable>[=number]</replaceable></option></term>
            <listitem><para>Set debugging level.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-v</option></term>
            <term><option>--verbose</option></term>
            <listitem><para>* Verbosely list any any errors encountered in the input files.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-?</option></term>
            <term><option>--help</option></term>
            <listitem><para>Display help and exit.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>--usage</option></term>
            <listitem><para>Display short usage message and exit.</para></listitem>
        </varlistentry>
        <varlistentry>
            <term><option>-V</option></term>
            <term><option>--version</option></term>
            <listitem><para>Display version information and exit.</para></listitem>
        </varlistentry>
        </variablelist>
    </refsection>
</refsection>

</refentry>

<!-- vim:set ts=4 sw=4 tw=120 et: -->
